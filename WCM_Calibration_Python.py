#------------------------------------------------------------------------------
## Water Cloud Model Calibration pour prairie
#------------------------------------------------------------------------------
"""

@author: Thomas DARDANT from Dipankar05
"""

"""
Spyder Editor

"""
## import de librairies et de packages
import numpy as np
import matplotlib
import matplotlib.pyplot as plt
from scipy.optimize import curve_fit
import pandas as pd
import warnings
from scipy.optimize import differential_evolution
from sklearn.metrics import r2_score
from sklearn.metrics import confusion_matrix


#------------------------------------------------------------------------------
## importation des données pour calibration
# un fichier X1 pour les données de calibrations contenant une colonne avec des valeurs de sigma 0 et d'humidité du sol
X1= pd.read_csv(r'D:\Ecole_documents\CRAW\Python\Sunshine\Data\SansInter_meteo_sansHiver\Assemblage\Calibration\VH_SI_TH_LA_BR.csv', header=None) ## Sigma 0 et humidité du sol
# un fichier Y1 pour les données de LAI
Y1= pd.read_csv(r'D:\Ecole_documents\CRAW\Python\Sunshine\Data\SansInter_meteo_sansHiver\Assemblage\LAI\LAI_SI_TH_LA_BR.csv', header=None) ## mesure de LAI

## importation des données pour prédiction
# un fichier X1_bis pour les données de calibrations contenant une colonne avec des valeurs de sigma 0 et d'humidité du sol
X1_bis= pd.read_csv(r'D:\Ecole_documents\CRAW\Python\Sunshine\Data\SansInter\Couple_SansInter\Calibration_Couple_VH_SansInter\VH_BI_SansInter.csv', header=None) ## Sigma 0 et humidité du sol
# un fichier Y1_bis pour les données de LAI
Y1_bis= pd.read_csv(r'D:\Ecole_documents\CRAW\Python\Sunshine\Data\SansInter\Couple_SansInter\LAI_Couple_SansInter\LAI_BI_SansInter.csv', header=None) ## mesure de LAI


#------------------------------------------------------------------------------
#Conversion des dataframes en matrice
# valeur de calibration
y=Y1.values
x=X1.values
# valeur de prédiction
x_bis=X1_bis.values
y_bis=Y1_bis.values
y_bis=y_bis[:,0]

# Définition de l'angle d'incidence des acquisition SAR
th=16 #angles en degré
thr=th*3.1415/180 #conversion en radian

# Extraction des valeurs des matrices de données calibrations
y=y[:,0]
x1=x[:,0]
x2=x[:,1]


#------------------------------------------------------------------------------
# Fonction pour exporter le DataFrame au format CSV
def export_df_to_csv(df, nom):
    """
    Exporte le DataFrame `df` au format CSV avec le nom spécifié.
    
    Args:
        df (pandas.DataFrame): DataFrame à exporter.
        nom (str): Nom du fichier CSV de sortie.
    """
    df.to_csv(nom, index=True)


#------------------------------------------------------------------------------
## fonction linéaire du WCM
def fitFunc(X,a,b,c,d,e):
    """
    Fonction modèle linéaire du WCM.

    Cette fonction représente un modèle mathématique utilisé dans le WCM pour
    ajuster des données. Elle prend en entrée un tuple de valeurs X et les paramètres
    du modèle (a, b, c, d, e, thr) et retourne une valeur estimée en fonction des
    paramètres et des données d'entrée.

    Args:
        X (tuple): Tuple de valeurs (x1, x2) utilisées comme entrées du modèle.
        a (float): Paramètre du modèle.
        b (float): Paramètre du modèle.
        c (float): Paramètre du modèle.
        d (float): Paramètre du modèle.
        e (float): Paramètre du modèle.
        thr (float): Paramètre du modèle, angle en radians.
    """
    x1,x2=X
    return (a*(np.power(x1,e))*np.cos(thr)*(1-np.exp((-2)*b*np.power((x1),1)/np.cos(thr))))+((d*np.exp(c*x2))*np.cos(thr)*np.exp((-2)*b*np.power((x1),1)/np.cos(thr)))


##-----------------------------------------------------------------------------
## Fonction pour minimiser les erreurs en utilisant la fonction du WCM
##Les limittes des paramètres sont fixé dans generate_Initial_Parameters()
def Error(parameterTuple):
    """
    Calcule l'erreur entre les données observées et les valeurs estimées par le modèle.

    Cette fonction calcule la racine carrée de la somme des carrés des différences entre
    les valeurs observées (y) et les valeurs estimées par le modèle (fitFunc).

    Args:
        parameterTuple (tuple): Tuple de paramètres du modèle passés à fitFunc.

    """
    warnings.filterwarnings("ignore") # do not print warnings by genetic algorithm
    return np.sqrt(np.sum((y - fitFunc((x1,x2), *parameterTuple)) ** 2).mean())


##-----------------------------------------------------------------------------
## fonction pour estimer les paramètres initial.
def generate_Initial_Parameters():
    """
   Génère des paramètres initiaux pour le modèle en utilisant l'algorithme de recherche différentielle.

   Cette fonction génère des valeurs initiales pour les paramètres du modèle en utilisant
   l'algorithme de recherche différentielle pour minimiser l'erreur entre les données observées
   et les valeurs estimées par le modèle.

   Returns:
       numpy.ndarray: Un tableau contenant les paramètres initiaux du modèle.
   """
    ## min et max utilisée pour les limites 
    parameterBounds = []
    parameterBounds.append([0,1.1]) # parameter bounds for a
    parameterBounds.append([0,0.5]) # parameter bounds for b
    parameterBounds.append([-0.5,1]) # parameter bounds for c
    parameterBounds.append([-0.5,1]) # parameter bounds for d
    parameterBounds.append([-1.5,1]) # parameter bounds for e
    ##parameterBounds.append([-100,100]) # parameter bounds for f
   

    ## "seed" le nombre aléatoire numpy pour répéter les résultats (test réalisé avec seed=3)
    ##https://docs.scipy.org/doc/scipy-0.15.1/reference/generated/scipy.optimize.differential_evolution.html
    result = differential_evolution(Error, parameterBounds, strategy='best1bin' ,polish=True,seed=3,init='latinhypercube')
    return result.x


##-----------------------------------------------------------------------------
## génération des valeurs des paramètres initiaux
initialParameters = generate_Initial_Parameters()
## ou utilisé des paramètre initiaux déjà connue
#initialParameters=[0.2,1.357,2,4,-1.965]


##-----------------------------------------------------------------------------
# Entrainement du modèle sur le jeu de données
fitParams,fitCovariances = curve_fit(fitFunc,(x1,x2),y, initialParameters,method='lm',maxfev=6000,ftol=1e-8)


##-----------------------------------------------------------------------------
#prediction des valeurs grâce à la fonction entrainer pour la calibration
A=x.T
ypred=fitFunc(A,fitParams[0],fitParams[1],fitParams[2],fitParams[3],fitParams[4])


##-----------------------------------------------------------------------------
#prediction des valeurs grâce à la fonction entrainer pour la prédiction
A_bis=x_bis.T
ypred_bis=fitFunc(A_bis,fitParams[0],fitParams[1],fitParams[2],fitParams[3],fitParams[4])
# ypred=fitFunc(A, 0.983,0.101,2.294,0.488,0.625)


##-----------------------------------------------------------------------------
#estimation du rmse
def rmse(predictions, targets):
    """
    Calcule la racine de l'erreur quadratique moyenne (RMSE) entre les prédictions et les cibles.

    Le RMSE est une mesure de la différence entre les valeurs prédites et les valeurs cibles.
    Il est calculé comme la racine carrée de la moyenne des carrés des différences entre
    les prédictions et les cibles.

    Args:
        predictions (numpy.ndarray): Tableau contenant les valeurs prédites.
        targets (numpy.ndarray): Tableau contenant les valeurs cibles.

    Returns:
        float: La valeur de la racine de l'erreur quadratique moyenne (RMSE).
    """
    return np.sqrt(((predictions - targets) ** 2).mean())

rmse_val = rmse(ypred, y)
print('RMSE=',rmse_val)


##-----------------------------------------------------------------------------
#estimation des coefficient de Correlation 
corrr=np.corrcoef(ypred,y)
rr= corrr[0,1]
r2= r2_score(y, ypred)
print('R=',rr)


###-----------------------------------------------------------------------------###
#erreur absolue
er_abs = abs(y - ypred)
er_abs = pd.DataFrame(er_abs)
export_df_to_csv(er_abs, 'export_erreur_abs_SI_TH_LA_BRcsv')
####


###-----------------------------------------------------------------------------###
plt.scatter(x_bis[:,0],ypred_bis)
plt.show()

## Tracé des données sur une figure pour sauvegarde 
plt.scatter(y,ypred)
plt.xlim([-1, 9])
plt.ylim([-1, 9])
# plt.xlabel("Observed $\sigma^0$") 
# plt.ylabel("Estimated $\sigma^0$")
plt.xlabel("Observed LAI")
plt.ylabel("Estimated LAI")
plt.title("LAI-VH-Grassland-parSI_TH_LA_BR")

# plt.plot([0, 0.3], [0, 0.3], 'k:')
plt.annotate('r = %.3f'%rr, xy=(-1,3.3))#round off upto 3decimals
plt.annotate('RMSE = %.3f'%rmse_val, xy=(-1,4))
plt.annotate('fitp = %.3f'%fitParams[0], xy=(-1,5.5))
plt.annotate('%.3f'%fitParams[1], xy=(2,5.5))
plt.annotate('%.3f'%fitParams[2], xy=(4,5.5))
plt.annotate('%.3f'%fitParams[3], xy=(-0.5,4.7))
plt.annotate('%.3f'%fitParams[4], xy=(2,4.7))
matplotlib.rcParams.update({'font.size': 20})

# Tracé de la droite y = x
x = np.linspace(0, 6, 100)  # Crée un tableau de 100 valeurs de 0 à 10
y1 = x
plt.plot(x, y1)
plt.show()


###-----------------------------------------------------------------------------###
# #estimation des coefficient de Correlation 
# corrr_bis=np.corrcoef(ypred_bis ,y_bis)
# rr_bis= corrr_bis[0,1]
# r2_bis= r2_score(y_bis, ypred_bis)
# print('R_bis=',rr_bis)

# plt.scatter(y_bis,ypred_bis)
# plt.xlim([-1, 6])
# plt.ylim([-1, 6])
# # plt.xlabel("Observed $\sigma^0$")
# # plt.ylabel("Estimated $\sigma^0$")
# plt.xlabel("Observed LAI")
# plt.ylabel("Estimated LAI")
# plt.title("LAI-VH-Grassland-par5_20")
# # plt.plot([0, 0.3], [0, 0.3], 'k:')
# plt.annotate('r = %.3f'%rr_bis, xy=(-1,3.3))#round off upto 3decimals
# plt.annotate('RMSE = %.3f'%rmse_val, xy=(-1,4))
# plt.annotate('fitp = %.3f'%fitParams[0], xy=(-1,5.5))
# plt.annotate('%.3f'%fitParams[1], xy=(2,5.5))
# plt.annotate('%.3f'%fitParams[2], xy=(4,5.5))
# plt.annotate('%.3f'%fitParams[3], xy=(-0.5,4.7))
# plt.annotate('%.3f'%fitParams[4], xy=(2,4.7))
# matplotlib.rcParams.update({'font.size': 20})

 
# # Tracé de la droite y = x
# x = np.linspace(0, 6, 100)  # Crée un tableau de 100 valeurs de 0 à 10
# y1 = x
# plt.plot(x, y1)
# plt.show()
# # plt.savefig('LAI_Grassland_stat2.png')


###-----------------------------------------------------------------------------###
## Affichage des paramètres d'entrainement déterminer
print('Fitted WCM coefficients =\n',fitParams)
