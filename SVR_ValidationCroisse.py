# -*- coding: utf-8 -*-
"""
Created on Mon Jun 19 08:18:25 2023

@author: thoma
"""

# -*- coding: utf-8 -*-
"""
Created on Tue Jun  6 14:32:29 2023

@author: thoma
"""

from sklearn.svm import SVR
from sklearn.model_selection import train_test_split
from sklearn.model_selection import GridSearchCV
from sklearn.preprocessing import StandardScaler
from sklearn.metrics import mean_squared_error
from sklearn.metrics import r2_score
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import math
from sklearn.preprocessing import MinMaxScaler


if __name__ == "__main__":
    ##### Chargement des fichiers #####
    # # Lecture du fichier CSV de LAI
    # # filepath_LAI = r"D:\Ecole_documents\CRAW\Python\SVR_SVM\Data\LAI_20219_G.csv"
    # filepath_LAI = r"D:\Ecole_documents\CRAW\Python\WCM\Data\LAI_2019\LAI_2019_stat.csv"
    # df_LAI = pd.read_csv(filepath_LAI,header=None)
    
    # # Lecture du fichier CSV de sigma vv
    # # filepath_sig_vv = r"D:\Ecole_documents\CRAW\Python\SVR_SVM\Data\Sigma0_vv_20219_G.csv"
    # filepath_sig_vv = r"D:\Ecole_documents\CRAW\Python\WCM\Data\Sigma0_VV_2019\Sigma0_VV_2019_stat5.csv"
    # df_sig_vv = pd.read_csv(filepath_sig_vv,header=None)
    
    
    ###-----------------------------------------------------------------------------###
    #### chargement des données ####
    # Préparation des données
    X = pd.read_csv(r"D:\Ecole_documents\CRAW\Python\SVR_SVM\Data\X_2022_SansHiver\Stat33.csv", header=None)# Vos caractéristiques d'entraînement (tableau 2D)
    y = pd.read_csv(r"D:\Ecole_documents\CRAW\Python\WCM\Data\2022\LAI_2022_SansHiver\LAI_2022_stat33.csv", header=None)## Vos valeurs cibles d'entraînement (tableau 1D)


    ###-----------------------------------------------------------------------------###
    liste_r = []
    # boucle de test pour validation croissée
    for i in range(1000):
        # séparation des données
        X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.4)
        
        #définition du modèle et entrainement
        model = SVR(kernel='poly', C=0.01 , epsilon=0.1, gamma=0.1)
        model.fit(X_train, y_train)
        
        #prédicition des données test
        y_pred = model.predict(X_test) 
        
        
        scaler = MinMaxScaler()
        X_train_scaled  = scaler.fit_transform(X_train)
        X_test_scaled = scaler.transform(X_test)
        
        y_pred2 = model.predict(X_test_scaled)
        
        y_pred = y_pred.tolist()
        
        y_test = np.array(y_test)
        y_test = y_test.tolist()
        
        
        # calcul mse et rmse
        mse = mean_squared_error(y_pred, y_test)
        rmse = np.sqrt(mse)
        
        i = []
        for j in range(len(y_pred)):
            i.append(y_pred[j])
        
        y_pred = np.array(i)
        
        a = []
        for k in range(len(y_test)):
            a.append(y_test[k][0])
        
        y_test = np.array(a)
       
        #Calcul des indices de correlation
        corrr = np.corrcoef(y_pred, y_test)
        rr = corrr[0,1]
        liste_r.append(rr)
    
    # calcul du r moyen
    rr_mean = np.mean(liste_r)
    
    
    ###-----------------------------------------------------------------------------###
    ### plot des figure pour voir les valeurs prédit par rapport à la réaliter
    # plt.scatter(X_train,y_train)
    # plt.show()
    
    # plt.scatter(X_test,y_test)
    # plt.show()
    
    plt.scatter(y_pred2, y_test)
    plt.xlabel("LAI original")
    plt.show()
      
    plt.scatter(y_test,y_pred)

    plt.xlabel("LAI original")
    plt.ylabel("LAI estime")
    plt.title("LAI-SVR-Grassland-Par33")
    plt.annotate('r_mean = %.3f'%rr_mean, xy=(0.5, 8.8))#round off upto 3decimals
    plt.annotate('r = %.3f'%rr, xy=(0.5, 7.8))#round off upto 3decimals
    plt.annotate('RMSE = %.3f'%rmse, xy=(0.5, 6.8))
    plt.annotate('MAE = %.3f'%mse, xy=(0.5, 5.8))
    
    ### Tracé de la droite y = x
    x = np.linspace(0, 10, 100)  # Crée un tableau de 100 valeurs de 0 à 10
    y = x
    plt.plot(x, y)
    plt.show()
    
    
    ###-----------------------------------------------------------------------------###
    # ##### Test des meilleurs paramètres #####
    # # Grille des hyperparamètres
    # param_grid = {
    #     'C': [0.1, 1, 10],
    #     'epsilon': [0.01, 0.1, 1],
    #     'kernel': ['rbf', 'linear', 'poly'],
    #     'gamma': [0.01, 0.05, 0.1]}
    
    # svr = SVR()
    # # Recherche par grille avec validation croisée
    # grid_search = GridSearchCV(estimator=svr, param_grid=param_grid, cv=4)
    # grid_search.fit(X_test, y_test)
    
    # # Meilleurs hyperparamètres
    # best_params = grid_search.best_params_
    
    
    