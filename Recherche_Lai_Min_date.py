# -*- coding: utf-8 -*-
"""
Created on Thu May 25 14:05:19 2023

@author: Thomas Dardant
"""

import pandas as pd
import numpy as np
import re



def find_LAI_min(df):
    """
   Cherche les valeurs minimales de l'indice LAI dans un DataFrame.

   Args:
       df (pandas.DataFrame): Le DataFrame contenant les données.

   Returns:
       tuple: Un tuple contenant deux listes. La première liste contient les valeurs minimales de LAI pour chaque ligne du DataFrame.
       La deuxième liste contient le nombre de valeurs par ligne du DataFrame.
   """
    # Supprime les colonnes indésirables
    df_ext = df.drop(['PARCEL_ID', 'tile', 'buf', 'pixels'], axis=1)
    # Sélectionne toutes les colonnes paires
    df_ext = df_ext.iloc[:, :140:2]
        
    # Calcule le minimum de chaque ligne en ignorant les valeurs manquantes
    return df_ext.min(axis=1, skipna=True).tolist(),df_ext.count(axis=1).tolist()


def add_LAI_min_to_df(liste_LAI_min, df, nom):
    """
    Ajoute une colonne "LAI_min" (ou "nbr_min") au DataFrame contenant les valeurs minimales de l'indice LAI.

    Args:
        liste_LAI_min (list): La liste des valeurs minimales de LAI.
        df (pandas.DataFrame): Le DataFrame auquel ajouter la colonne.
        nom (str): Le nom de la colonne à ajouter.

    Returns:
        None
    """
    # Insère la colonne "LAI_min" dans le dataframe
    df.insert(4, nom, liste_LAI_min)
 
                
def export_df_to_csv(df, nom):
    """
    Exporte le DataFrame au format CSV.

    Args:
        df (pandas.DataFrame): Le DataFrame à exporter.
        nom (str): Le nom du fichier CSV de sortie.

    Returns:
        None
    """
    # Exporte le dataframe au format CSV
    df.to_csv(nom, index=False)


def find_date_of_LAI_min(df, liste_LAI_min):
    """
    Recherche les dates correspondantes aux valeurs minimales de l'indice LAI dans un DataFrame.

    Args:
        df (pandas.DataFrame): Le DataFrame contenant les données.
        liste_LAI_min (list): La liste des valeurs minimales de LAI.

    Returns:
        list: Une liste de noms de colonnes contenant les dates correspondantes aux valeurs minimales de LAI.
    """
    columnNames = []
    # Supprime les colonnes indésirables
    df = df.drop(['PARCEL_ID', 'tile', 'buf', 'pixels'], axis=1)
    # Sélectionne toutes les colonnes paires
    df = df.iloc[:, :]
    for elt in liste_LAI_min:
        a = df.isin([elt])
        h =(a.any()[a.any() == True].index).values
        if h == []:
            columnNames.append('LAI_min')
        else:
           columnNames.append((a.any()[a.any() == True].index).values)
        
    return columnNames


def creat_list_date(columnNames):
    """
    Crée une liste de dates en supprimant le texte "LAImoy" des noms de colonnes.

    Args:
        columnNames (list): La liste des noms de colonnes.

    Returns:
        list: La liste des dates correspondantes aux noms de colonnes modifiés.
    """
    liste_val = []
    for i in range(len(columnNames)):
        string = re.sub("LAImoy", "", columnNames[i][1])
        # Utilisation de la fonction re.sub pour supprimer le texte "LAImoy" de la deuxième valeur de chaque élément de columnNames
        # La fonction re.sub remplace toutes les occurrences du motif "LAImoy" par une chaîne de caractères vide
        liste_val.append(string)
        # Ajout de la valeur modifiée à la liste liste_val

    return liste_val
    # Retourne la liste liste_val contenant les valeurs modifiées


 
if __name__ == "__main__":
    
    # # Lecture du premier fichier CSV test
    # filepath = r"D:\Ecole_documents\CRAW\Data\fraction_LAImin2.csv"
    # df2 = pd.read_csv(filepath)

    
    # # Lecture du deuxième fichier CSV complet
    # filepath2 = r"D:\Ecole_documents\CRAW\Data\testexport2022.csv"
    # df2 = pd.read_csv(filepath2)
    
    
    # Lecture du premier fichier CSV test moyen taille
    filepath = "fraction_LAImin2.csv"
    df = pd.read_csv(filepath)
        
    
    # Calcul et ajout de la colonne "LAI_min"
    liste_LAI_min, compt = find_LAI_min(df)
    
    #ajout de la colonne compt
    nom_compt = 'nbr_min'
    add_LAI_min_to_df(compt, df, nom_compt)
        
    
    nom = 'LAI_min'
    add_LAI_min_to_df(liste_LAI_min, df, nom)
    
    # recherche des dates des LAI min
    columnNames = find_date_of_LAI_min(df, liste_LAI_min)
    liste_date = creat_list_date(columnNames)
    
    #ajout des dates dans le dataframe
    nom2 = 'Date_LAI_min'
    add_LAI_min_to_df(liste_date, df, nom2)
    
    # Export du nouveau DataFrame dans un fichier CSV
    nom_export1 = 'export_test_LAI_min_all.csv'
    export_df_to_csv(df, nom_export1)
    
   
    
   