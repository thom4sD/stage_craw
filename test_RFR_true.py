# -*- coding: utf-8 -*-
"""
Created on Mon Jun 19 14:22:30 2023

@author: thoma
"""
###-----------------------------------------------------------------------------###
# Importer les bibliothèques nécessaires
from sklearn.ensemble import RandomForestRegressor
from sklearn.model_selection import train_test_split
from sklearn.metrics import mean_squared_error
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt


###-----------------------------------------------------------------------------###
# Charger les données d'entraînement
X = pd.read_csv(r"D:\Ecole_documents\CRAW\Python\SVR_SVM\Data\X_2022_SansHiver\Stat33.csv", header=None)# Vos caractéristiques d'entraînement (tableau 2D)
y = pd.read_csv(r"D:\Ecole_documents\CRAW\Python\WCM\Data\2022\LAI_2022_SansHiver\LAI_2022_stat33.csv", header=None)# Vos valeurs cibles d'entraînement (tableau 1D)


###-----------------------------------------------------------------------------###
### boucle pour multit tests validation croisées
liste_r = []
for i in range(100):
    # Séparer les données en ensembles d'entraînement et de test
    X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.2)
    
    # Créer l'estimateur de la régression forestière aléatoire
    regressor = RandomForestRegressor(n_estimators=100, random_state=42)
    
    # Entraîner le modèle sur les données d'entraînement
    regressor.fit(X_train, y_train)
    
    # Faire des prédictions sur les données de test
    y_pred = regressor.predict(X_test)
    
    # Évaluer les performances du modèle en utilisant la métrique de votre choix
    mse = mean_squared_error(y_test, y_pred)
    rmse = np.sqrt(mse)
    
    #Mise en forme des données
    y_pred = y_pred.tolist()
    y_test = np.array(y_test)
    y_test = y_test.tolist()
    i = []
    for j in range(len(y_pred)):
        i.append(y_pred[j])
    
    y_pred = np.array(i)
    
    a = []
    for k in range(len(y_test)):
        a.append(y_test[k][0])
    
    y_test = np.array(a)
    corrr = np.corrcoef(y_pred, y_test)
    rr = corrr[0,1]

    liste_r.append(rr)


###-----------------------------------------------------------------------------###
### affichage des graphiques et des données
rr_mean = np.mean(liste_r)
plt.scatter(y_test,y_pred)
plt.xlabel("LAI original")
plt.ylabel("LAI estime")
plt.title("LAI-RFR-Grassland-Par33")
plt.annotate('r_mean = %.3f'%rr_mean, xy=(0.5, 8.8))#round off upto 3decimals
plt.annotate('r = %.3f'%rr, xy=(0.5, 7.8))#round off upto 3decimals
plt.annotate('RMSE = %.3f'%rmse, xy=(0.5, 6.8))
plt.annotate('MAE = %.3f'%mse, xy=(0.5, 5.8))
# Tracé de la droite y = x
x = np.linspace(0, 10, 100)  # Crée un tableau de 100 valeurs de 0 à 10
y = x
plt.plot(x, y)
plt.show()


###-----------------------------------------------------------------------------###
### affichage des erreurs
print("Mean Squared Error:", mse)
print('rr: ', rr_mean)