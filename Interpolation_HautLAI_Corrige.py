# -*- coding: utf-8 -*-
"""
Created on Wed May 31 09:11:08 2023

@author: Thomas Dardant
"""

import pandas as pd
import numpy as np
import ephem
import datetime


# Fonction pour remplir le DataFrame avec les valeurs d'un autre DataFrame
def remplir_df(df_a_remplir, df_val):
    """
   Remplit le DataFrame `df_a_remplir` avec les valeurs d'un autre DataFrame `df_val`.
   
   Args:
       df_a_remplir (pandas.DataFrame): DataFrame à remplir.
       df_val (pandas.DataFrame): DataFrame contenant les valeurs à insérer.
   
   Returns:
       pandas.DataFrame: DataFrame `df_a_remplir` rempli avec les valeurs de `df_val`.
   """
    n, m = np.shape(df_val)
    for i in range(n):
        df_ext = df_val.iloc[i]
        sat = df_ext.iat[0]
        date = df_ext.iat[1]
        val = df_ext.iat[2]
        df_a_remplir.loc[sat, date] = val
    return df_a_remplir


# Fonction pour exporter le DataFrame au format CSV
def export_df_to_csv(df, nom):
    """
    Exporte le DataFrame `df` au format CSV avec le nom spécifié.
    
    Args:
        df (pandas.DataFrame): DataFrame à exporter.
        nom (str): Nom du fichier CSV de sortie.
    """
    df.to_csv(nom, index=True)
    

def recup_val_haut(df_val, df_a_remplir):
    """
   Récupère les valeurs de hauteur moyenne à partir de `df_val` et les insère dans `df_a_remplir`.
   Certaines valeurs sont remplacées par 'nan' en fonction de certaines conditions.
   
   Args:
       df_val (pandas.DataFrame): DataFrame contenant les valeurs de hauteur moyenne.
       df_a_remplir (pandas.DataFrame): DataFrame dans lequel insérer les valeurs de hauteur moyenne.
   
   Returns:
       pandas.DataFrame: DataFrame `df_a_remplir` avec les valeurs de hauteur moyenne insérées.
   """
    columns = df_a_remplir.columns
    index = df_a_remplir.index
    df_a_remplir = pd.DataFrame(df_a_remplir)
    
    n = len(index)
    m = len(columns)

    for i in range(n):
        for j in range(m):
            pos1 = index[i]
            pos2 = columns[j]
            if pos2 <= 59594.0:
                val = 'nan'
            else:
                val = df_val.loc[pos1, pos2]
            df_a_remplir.loc[pos1, pos2] = val

    return df_a_remplir


#fonction qui crée un nouveau DataFrame en concaténant les valeurs des DataFrames
def creat_LAI_Haut(df1, df2):
    """
    Crée un nouveau DataFrame en concaténant les valeurs des DataFrames `df1` et `df2` dans des colonnes spécifiques.
    
    Args:
        df1 (pandas.DataFrame): Premier DataFrame.
        df2 (pandas.DataFrame): Deuxième DataFrame.
    
    Returns:
        pandas.DataFrame: Nouveau DataFrame créé en concaténant les valeurs de `df1` et `df2`.
    """
    columns = df1.columns
    index = df2.index
    n = len(index)
    m = len(columns)
    df_retour = pd.DataFrame(columns=['ID_sat','Date','hauteur_moy','LAI'])
    
    for i in range(n):
        for j in range(m):
            val1 = df1.iloc[i,j]
            val2 = df2.iloc[i,j]
            ind = index[i]
            col = columns[j]
            df_new_row = pd.DataFrame(data=np.array([[ind,col,val1,val2]]), columns=['ID_sat','Date','hauteur_moy','LAI'])
            df_retour = pd.concat([df_retour,df_new_row], ignore_index=True)
    
    return df_retour


# Convertir une date en jour julien modifié
def convert_to_modified_julian_day(date):
    """
    Convertit une date en jour julien modifié.
    
    Args:
        date (datetime.datetime): Date à convertir.
    
    Returns:
        float: Jour julien modifié correspondant à la date.
    """
    jd = ephem.julian_date(date)
    mjd = jd - 2400000.5
    return mjd
    
    

def liste_colonne(liste):
    """
    Convertit les dates dans une liste en jour julien modifié en utilisant la fonction `convert_to_modified_julian_day`.
    
    Args:
        liste (list): Liste de dates à convertir.
    
    Returns:
        numpy.ndarray: Liste de dates converties en jour julien modifié.
    """
    for i in range(len(liste)):
        date_string = liste[i]
        date = ephem.Date(date_string)
        mjd = convert_to_modified_julian_day(date)
        liste[i] = mjd
    
    return liste


#Focntion qui vérifie si la valeur a été interpolé ou non 
def verif_interpol(a1, a2, b1, b2):
    """
    Vérifie l'interpolation des valeurs en comparant deux listes de colonnes.
    
    Args:
        a1 (numpy.ndarray): liste des dates pour les valeurs à tester.
        a2 (numpy.ndarray): liste des parcelles pour les valeurs à tester.
        b1 (numpy.ndarray): liste des dates pour les valeurs de références.
        b2 (numpy.ndarray): liste des parcelles pour les valeurs de références.
    
    Returns:
        list: Liste contenant '1' si l'interpolation est nécessaire et '0' sinon pour chaque colonne à vérifier.
    """
    liste_inter = []
    
    c = zip(a1, a2)
    d = zip(b1, b2)
    t = tuple(c)
    e = tuple(d)
    
    for elt in t:
        if elt in e:
            liste_inter.append(0)
        else:
            liste_inter.append(1)
    
    return liste_inter

# Fonction qui convertie une date en jours julien modifie en date y_m_d
def convert_to_date(modified_julian_day):
    """
    Converts a modified Julian day to a date in the format 'yyyy-mm-dd'.

    Args:
        modified_julian_day (float): The modified Julian day to convert.

    Returns:
        str: The converted date in the format 'yyyy-mm-dd'.
    """
    base_date = datetime.datetime(1858, 11, 17)  # Date de référence pour le jour julien modifié
    delta = datetime.timedelta(days=modified_julian_day)  # Calcul du décalage par rapport à la date de référence
    target_date = base_date + delta  # Date cible en ajoutant le décalage
    return target_date.strftime("%Y-%m-%d")  # Conversion de la date en format aaaa-mm-jj

    

if __name__ == "__main__":
    # Lecture du fichier CSV de LAI
    filepath_LAI = "LAI_Sunshine_extrait.csv"
    df_LAI = pd.read_csv(filepath_LAI)
    df_LAI.set_index('ID', inplace=True)
    
    # Lecture du fichier CSV de Hauteur Moy
    filepath_Hauteur = "Valeur_HauteurH_Sunshine.csv"
    df_Hauteur = pd.read_csv(filepath_Hauteur)
    
    # Lecture du fichier CSV de clean
    filepath_clean = "Df_clean_Hateur.csv"
    df_clean = pd.read_csv(filepath_clean)
    df_clean.set_index('ID_sat', inplace=True)
    
    # Remplissage du dataframe
    df_clean_hauteur = remplir_df(df_clean, df_Hauteur)
    
    # Vidange du dataframe pour récupérer les dates des acquisitions LAI
    df_vide = df_LAI.drop(index=df_LAI.index)
    
    # Extraction des date de LAI
    liste_jour_LAI = df_vide.columns.values
    liste_jour_LAI = liste_colonne(liste_jour_LAI)
    liste_jour_LAI= liste_jour_LAI.reshape((len(liste_jour_LAI),1))
    
    # Extraction des date d'acquisition des hauteurs
    liste_jour_Haut = df_clean.columns.values
    liste_jour_Haut = liste_colonne(liste_jour_Haut)
    liste_jour_Haut = liste_jour_Haut.reshape((len(liste_jour_Haut),1))
    
    # Concatenaion des jours pour avoir une échelle de temps complète
    liste_jour = np.concatenate((liste_jour_Haut, liste_jour_LAI))
    liste_jour.sort()
    
    # Linéarisation de l'échelle du temps 
    indice1 = liste_jour[0][0]
    indice2 = liste_jour[-1][0]
    lim = indice2 - indice1
    x = np.linspace(indice1, indice2, num=int(lim), dtype=int)
    x = x.astype(float)
    
    ####
    liste_col_LAI = df_LAI.columns.values
    liste_col_LAI = liste_col_LAI.astype(float)
    x_liste = x.tolist()

    liste_col_LAI = liste_col_LAI.tolist()
    for elt in liste_col_LAI:
        if elt in x_liste:
            x_liste.remove(elt)

    x = np.array(x_liste)
    #####
    
    x_str = x.astype(str)
    # Création d'un dataframe avec toute les valeur de temps
    df_test3 = pd.DataFrame(columns=x_str)
    
    # Concaténation des DataFrames
    concat = pd.concat([df_LAI, df_test3], axis=1)  
    concat = concat.astype(float)
    concat.columns = np.array(concat.columns.values).astype(str)
    concat = concat.transpose()
    
    # Trie des indexs du dataframe et on retire les valeurs en doubles
    concat = concat.sort_index()
    # concat = concat[concat.index.duplicated()]
    
    # Interpolation des valeurs de temps pour obtenir une valeur a chaque instant
    concat_interpolated = concat.interpolate(limit_area = 'inside')
    concat_interpolated = concat_interpolated.transpose()
    concat_interpolated.columns = np.array(concat_interpolated.columns.values).astype(float)
    concat_interpolated.index = np.array(concat_interpolated.index.values).astype(str)
    
    # Copie de df_LAI
    df_copie = df_LAI.copy()
    # df_copie = df_copie.astype(float)
    df_copie.columns = np.array(df_copie.columns.values).astype(float)
    df_copie.index = np.array(df_copie.index.values).astype(str)
    df_recup_val = recup_val_haut(concat_interpolated, df_copie)
    
    # Création du fichier de sortie LAI Hauteur_moy
    df_retour = creat_LAI_Haut(df_recup_val, df_LAI)
    
    
    # conversion des date jour julien modifie en y-m-d
    a = df_retour['Date'].values.astype(float)
    dates = []
    for elt in a:
        date = convert_to_date(elt)
        dates.append(date)
    
    df_retour.insert(2,'Date_ymd',dates)
     
    
    a1 = df_retour['Date_ymd'].values.astype(str)
    a2 = df_retour['ID_sat'].values.astype(int)
    
    b1 = df_Hauteur['Date'].values.astype(str)
    b2 = df_Hauteur['ID_sat'].values.astype(int)
    
    # Vérification de l'interpolation des valeurs
    liste_inter = verif_interpol(a1, a2, b1, b2)
    df_retour = df_retour.assign(interpol=liste_inter)
    
   
    # Sauvegarde du dataframe créé
    export_df_to_csv(df_retour, 'export_valeur_interpole_LAIHAUT_1807_inv.csv')

    df_verifinter = pd.read_csv("export_valeur_interpole_LAIHAUT_1807_verif3.csv")
    
    LAI = np.array(df_verifinter[['LAI']])

    LAI = LAI[:,0]

    LAI_inter = np.array(df_verifinter[['LAI_interpolated']])

    LAI_inter = LAI_inter[:,0]

    liste_inter = []
    for i in range(len(LAI)):
        if LAI[i] == LAI_inter[i]:
            liste_inter.append(0)
        else:
            liste_inter.append(1)
    
    df_verifinter["interpol_LAI"] = liste_inter
        
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    